Courses recomendation

Рекомендательная система образовательных курсов
1. Выкачены данные с Coursera, Stepic, Openedu, Htmlacademy
2.  Переведены на русский и на английский
3. Выкачены теги с researchgate.net
4. Переведены на русский и на Английский
5. Получены новые теги специальным алгоритмом(брал слова, которые стоят рядом с ключевыми словами, близкими в word2vec пространстве со словом requirements)
6. Протегированы все курсы и вакансии
7. Сделана матрица Курсы/Вакансии, в каждой ячейке которой находится динна пересечения множества тегов курса и множества тегов вакансии, деленная на длину объединения
8. Сделан прототип рекомендательной системы Вакансия->Курс(Алгоритм SVD разложения)
9. Написана демо версия web-приложения рекомендательной системы(для сбора новых данных)